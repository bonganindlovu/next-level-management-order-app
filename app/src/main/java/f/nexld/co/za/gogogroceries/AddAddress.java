package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.saveDeviceAddress;
import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.saveDeviceEmail;

public class AddAddress extends AppCompatActivity {
    EditText business_name,building_name,street_number,suburb,city;
    Spinner payment_method,delivery_method;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        building_name = findViewById(R.id.building_name);
        business_name = findViewById(R.id.business_name);
        street_number = findViewById(R.id.street_number);
        suburb = findViewById(R.id.suburb);
        city = findViewById(R.id.city);
        payment_method = findViewById(R.id.payment_method);
        delivery_method = findViewById(R.id.delivery_method);

        String device_id = MyPreferences.getDeviceID(this);
        getCartTotals(device_id);

    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void saveAddress(View view) {
        String b_name = business_name.getText().toString();
        String c_name = building_name.getText().toString();
        String s_number = street_number.getText().toString();
        String t_suburb = suburb.getText().toString();
        String t_city = city.getText().toString();
        String pay = String.valueOf(payment_method.getSelectedItem());
        String del = String.valueOf(delivery_method.getSelectedItem());

        String myLocation = s_number+", "+t_suburb+", "+t_city;
        String latlong = null;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocationName(myLocation, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = addresses.get(0);
        if(addresses.size() > 0) {

            double latitude = addresses.get(0).getLatitude();
            double longitude = addresses.get(0).getLongitude();

            latlong = String.valueOf(latitude)+","+String.valueOf(longitude);
        }
        
        
       

        if (s_number.isEmpty()){
            //empty
            Toast.makeText(AddAddress.this, "Street number is required!", Toast.LENGTH_SHORT).show();
        }
        else if ( t_suburb.isEmpty()){
            Toast.makeText(AddAddress.this, "Suburb is required!", Toast.LENGTH_SHORT).show();
        }
        else if (t_city.isEmpty()){
            Toast.makeText(AddAddress.this, "City is required!", Toast.LENGTH_SHORT).show();

        }
        else if (pay.equalsIgnoreCase("Select Payment Method")){
            Toast.makeText(AddAddress.this, "Please select payment method!", Toast.LENGTH_SHORT).show();

        }
        else if (del.equalsIgnoreCase("Select Delivery Method")){
            Toast.makeText(AddAddress.this, "Please select delivery method!", Toast.LENGTH_SHORT).show();
        }
        else{
            
            addA(b_name,c_name,s_number,t_suburb,t_city,pay,latlong,del);

        }
    }

    public void addA(final String b_name,final String c_name,final String s_number,final String t_suburb,final String t_city,final String pay,final String gps,final String del) {

        RequestQueue requestQueue = Volley.newRequestQueue(AddAddress.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.ADD_ADDRESS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String status = jsonObject1.getString("status");
                    String user_email = jsonObject1.getString("user_email");
                    String message = jsonObject1.getString("message");

                    if (status.equalsIgnoreCase("passed")) {
                        Toast.makeText(AddAddress.this, "" + message, Toast.LENGTH_SHORT).show();
                        //go to the next page
                        saveDeviceAddress(AddAddress.this,"saved");
                        Intent in = getIntent();
                        String next_page = in.getExtras().getString("next_page");
                        //go to the next page
                        if (next_page.equalsIgnoreCase("check")){
                            //go to check
                            Intent intent= new Intent(AddAddress.this, Checkout.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            finish();
                        }
                        else if (next_page.equalsIgnoreCase("orders")){
                            //go to orders
                            Intent intent= new Intent(AddAddress.this, Orders.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        else if (next_page.equalsIgnoreCase("account")){
                            //go to account
                            Intent intent= new Intent(AddAddress.this, Account.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();

                        }
                        else{
                            //go to main
                            Intent intent= new Intent(AddAddress.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            finish();

                        }
                    }
                    else{
                        Toast.makeText(AddAddress.this, "" + message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(AddAddress.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                String email = MyPreferences.getUserEmail(AddAddress.this);
                stringMap.put("email", email);
                stringMap.put("business_name", b_name);
                stringMap.put("building_name", c_name);
                stringMap.put("street_number", s_number);
                stringMap.put("suburb", t_suburb);
                stringMap.put("city", t_city);
                stringMap.put("payment_method", pay);
                stringMap.put("delivery_method", del);
                stringMap.put("gps", gps);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(AddAddress.this);
        progressDialog.setMessage("Saving Address...");
        progressDialog.show();

    }
    public void getCartTotals(final String device_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(AddAddress.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_CHECK_TOTALS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String total_price_amount = jsonObject1.getString("total_price");
                    String total_items_amount = jsonObject1.getString("total_items");
                    String delivery = jsonObject1.getString("delivery");
                    String total_invoice = jsonObject1.getString("total_invoice");
                    String payment_meth = jsonObject1.getString("payment_method");
                    String delivery_addr = jsonObject1.getString("delivery_address");
                    String delivery_suburb = jsonObject1.getString("delivery_suburb");
                    String delivery_city = jsonObject1.getString("delivery_city");
                    String bus_name = jsonObject1.getString("business_name");
                    String build_name = jsonObject1.getString("building_name");




                    street_number.setText(delivery_addr);
                    suburb.setText(delivery_suburb);
                    city.setText(delivery_city);
                    business_name.setText(bus_name);
                    building_name.setText(build_name);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                String email = MyPreferences.getUserEmail(AddAddress.this);
                stringMap.put("user_id", email);
                stringMap.put("device_id", device_id);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }

}
