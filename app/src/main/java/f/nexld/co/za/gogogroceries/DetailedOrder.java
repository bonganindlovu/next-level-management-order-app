package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.adapters.AdapterCartProducts;
import f.nexld.co.za.nextleveldistribution.adapters.AdapterDetailItems;
import f.nexld.co.za.nextleveldistribution.data.DataCartItems;
import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

public class DetailedOrder extends AppCompatActivity {
    TextView order_number,order_date,order_total,suburb,city,business_name,building_name,street_number,
            delivery_method,payment_method,delivery_total,order_status,track_order,
            order_sub_total,order_vat_total;
    private AdapterDetailItems adapterCart;
    RecyclerView saleRecycle;
    NestedScrollView nest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_order);

        order_number = findViewById(R.id.order_number);
        order_date =findViewById(R.id.order_date);
        order_total = findViewById(R.id.order_total);
        saleRecycle = (RecyclerView) findViewById(R.id.detailRecycle);
        nest = (NestedScrollView)findViewById(R.id.nest) ;
        track_order = findViewById(R.id.track_order);
        order_sub_total = findViewById(R.id.order_sub_total);
        order_vat_total = findViewById(R.id.order_vat_total);



        street_number =findViewById(R.id.street_number);
        suburb=findViewById(R.id.suburb);
        city = findViewById(R.id.city);
        business_name = findViewById(R.id.business_name);
        building_name = findViewById(R.id.building_name);

        payment_method = findViewById(R.id.payment_method);
        delivery_method = findViewById(R.id.delivery_method);
        delivery_total = findViewById(R.id.delivery_total);
        order_status= findViewById(R.id.order_status);

        Intent in = getIntent();
        String torder_number = in.getExtras().getString("order_id");

        getOrderTotals(torder_number);
        loadOrderItems(torder_number);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }


    public void getOrderTotals(final String torder_number) {

        RequestQueue requestQueue = Volley.newRequestQueue(DetailedOrder.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_ORDER_TOTALS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String torder_number = jsonObject1.getString("order_number");
                    String torder_date = jsonObject1.getString("order_date");
                    String torder_total = jsonObject1.getString("order_total");
                    String torder_status = jsonObject1.getString("order_status");

                    String tpayment_method = jsonObject1.getString("payment_method");
                    String tdelivery_method = jsonObject1.getString("delivery_method");

                    String tstreet_number = jsonObject1.getString("street_number");
                    String tsuburb = jsonObject1.getString("suburb");
                    String tcity = jsonObject1.getString("city");
                    String tbusiness_name = jsonObject1.getString("business_name");
                    String tbuilding_name = jsonObject1.getString("building_name");
                    String tdelivery_total = jsonObject1.getString("delivery_total");

                    String tsub_total = jsonObject1.getString("sub_total");
                    String tvat_total = jsonObject1.getString("vat_total");

                    order_sub_total.setText("R "+tsub_total);
                    order_vat_total.setText("R "+tvat_total);

                    payment_method.setText(tpayment_method);
                    delivery_method.setText(tdelivery_method);
                    street_number.setText(tstreet_number);
                    suburb.setText(tsuburb);
                    city.setText(tcity);
                    business_name.setText(tbusiness_name);
                    building_name.setText(tbuilding_name);
                    delivery_total.setText(tdelivery_total);
                    order_status.setText("Status: "+torder_status);
                    if (torder_status.equalsIgnoreCase("On the way")){
                        track_order.setVisibility(View.VISIBLE);
                    }
                    else{
                        track_order.setVisibility(View.GONE);
                    }

                    order_date.setText(torder_date);
                    order_number.setText("ORDER#: "+torder_number);
                    order_total.setText("R "+torder_total);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("order_number", torder_number);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }

    private void loadOrderItems(final String torder_number) {

        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.LOAD_ORDERS_ITEMS+"?order_number="+torder_number,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Cart.this, "LINK: "+result, Toast.LENGTH_SHORT).show();
                        nest.setVisibility(View.VISIBLE);

                        try {
                            JSONArray jArray = new JSONArray(result);


                            List<DataCartItems> data = new ArrayList<>();


                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataCartItems proData = new DataCartItems();

                                proData.productId = json_data.getString("product_id");
                                proData.productName = json_data.getString("product_name");
                                proData.productPrice = json_data.getString("original_price");
                                proData.productQuantity = json_data.getString("product_quantity");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();
                            }


                            LinearLayoutManager layoutManager = new LinearLayoutManager(DetailedOrder.this);
                            adapterCart = new AdapterDetailItems(DetailedOrder.this, data);
                            saleRecycle.setHasFixedSize(false);
                            saleRecycle.setAdapter(adapterCart);
                            saleRecycle.setLayoutManager(layoutManager);
                            adapterCart.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        nest.setVisibility(View.GONE);
                        Toast.makeText(DetailedOrder.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void trackOrder(View view) {
    }
}
