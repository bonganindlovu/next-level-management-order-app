package f.nexld.co.za.nextleveldistribution.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.DetailedOrder;
import f.nexld.co.za.nextleveldistribution.ProductDetails;
import f.nexld.co.za.nextleveldistribution.R;
import f.nexld.co.za.nextleveldistribution.data.DataCartItems;
import f.nexld.co.za.nextleveldistribution.data.DataItems;
import f.nexld.co.za.nextleveldistribution.data.DataOrders;

/**
 * Created by DEBONGZ on 4/8/2018.
 */

public class AdapterOrders extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataOrders> data= Collections.emptyList();
    DataItems current;
    int currentPos=0;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterOrders(Context context, List<DataOrders> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_orders_items, parent,false);
        AdapterOrders.MyHolder holder=new AdapterOrders.MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final AdapterOrders.MyHolder myHolder= (AdapterOrders.MyHolder) holder;
        final DataOrders current=data.get(position);

        myHolder.text_order_number.setText("ORDER#: "+current.orderId);
        myHolder.text_order_date.setText(current.orderDate);
        myHolder.text_order_price.setText("R "+current.productPrice);
        myHolder.text_order_items.setText("QTY: "+current.productQuantity);
        myHolder.text_order_status.setText(current.orderStatus);

        final String product_id = current.orderId;
        myHolder.mainRaiderLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent= new Intent(context, DetailedOrder.class);
                intent.putExtra("order_id", product_id);
                context.startActivity(intent);

            }
        });

    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        TextView text_order_number;
        TextView text_order_date;
        TextView text_order_price;
        TextView text_order_items,text_order_status;
        RelativeLayout mainRaiderLine;




        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            text_order_number = (TextView) itemView.findViewById(R.id.order_number);
            text_order_date= (TextView) itemView.findViewById(R.id.order_date);
            mainRaiderLine =(RelativeLayout)itemView.findViewById(R.id.mainraider_line);
            text_order_price = (TextView) itemView.findViewById(R.id.order_price);
            text_order_items = (TextView) itemView.findViewById(R.id.order_items);
            text_order_status = (TextView) itemView.findViewById(R.id.order_status);
        }


        @Override
        public void onClick(View v) {

        }
    }



}
