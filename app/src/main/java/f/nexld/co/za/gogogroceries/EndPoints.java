package f.nexld.co.za.nextleveldistribution;

/**
 * Created by DEBONGZ on 4/20/2018.
 */

public class EndPoints {
    public static final String HOME_PRODUCTS ="http://nextld.co.za/salesadmin/app/nld_products.php";

    public static final String HOME_PRODUCTS_NEW ="http://nextld.co.za/salesadmin/app/nld/nld_products.php";
    public static final String ADD_TO_CART ="http://nextld.co.za/salesadmin/app/nld/add_to_cart.php";
    public static final String REMOVE_ITEM ="http://nextld.co.za/salesadmin/app/nld/remove_item.php";


    public static final String ALL_PRODUCTS ="http://nextld.co.za/salesadmin/app/nld/nld_all_products.php";

    public static final String PRODUCTS ="http://nextld.co.za/salesadmin/app/nld/products.php";



    public static final String LOAD_CART="http://nextld.co.za/salesadmin/app/nld/load_cart.php";
    public static final String LOAD_CART_TOTALS="http://nextld.co.za/salesadmin/app/nld/load_cart_totals.php";

    public static final String LOAD_CHECK_TOTALS="http://nextld.co.za/salesadmin/app/nld/load_check_totals.php";

    public static final String SEND_ORDER="http://nextld.co.za/salesadmin/app/nld/send_order.php";

    public static final String REGISTER="http://nextld.co.za/salesadmin/app/nld/register.php";
    public static final String LOG_IN="http://nextld.co.za/salesadmin/app/nld/log_in.php";
    public static final String UPDATE_PROFILE ="http://nextld.co.za/salesadmin/app/nld/update_profile.php";

    public static final String LOAD_ORDERS="http://nextld.co.za/salesadmin/app/nld/load_orders.php";
    public static final String LOAD_ORDERS_ITEMS="http://nextld.co.za/salesadmin/app/nld/load_orders_items.php";

    public static final String LOAD_ORDER_TOTALS="http://nextld.co.za/salesadmin/app/nld/load_order_totals.php";
    public static final String ADD_ADDRESS="http://nextld.co.za/salesadmin/app/nld/add_address.php";
    public static final String LOAD_PROFILE="http://nextld.co.za/salesadmin/app/nld/load_user_profile.php";
    public static final String GET_ORDER_TOTALS="http://nextld.co.za/salesadmin/app/nld/get_order_total.php";
    public static final String RESET_PASSWORD ="http://nextld.co.za/salesadmin/app/nld/reset_password.php";
}

