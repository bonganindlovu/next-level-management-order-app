package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.adapters.AdapterCartProducts;
import f.nexld.co.za.nextleveldistribution.adapters.AdapterOrders;
import f.nexld.co.za.nextleveldistribution.data.DataCartItems;
import f.nexld.co.za.nextleveldistribution.data.DataOrders;
import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

public class Orders extends AppCompatActivity {
    private AdapterOrders adapterOrders;
    RecyclerView saleRecycle;
    TextView error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);
        saleRecycle = findViewById(R.id.ordersRecycle);
        error = findViewById(R.id.error);

        String email = MyPreferences.getUserEmail(this);
        loadOrders(email);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    private void loadOrders(final String email) {

        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.LOAD_ORDERS+"?email="+email,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Cart.this, "LINK: "+result, Toast.LENGTH_SHORT).show();
                        if (result.equalsIgnoreCase("no_results")){
                            error.setVisibility(View.VISIBLE);
                            saleRecycle.setVisibility(View.GONE);
                        }
                        else{
                            saleRecycle.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);

                        }
                        try {
                            JSONArray jArray = new JSONArray(result);


                            List<DataOrders> data = new ArrayList<>();


                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataOrders proData = new DataOrders();

                                proData.orderId = json_data.getString("order_id");
                                proData.orderDate = json_data.getString("order_date");
                                proData.productPrice = json_data.getString("order_price");
                                proData.productQuantity = json_data.getString("order_quantity");
                                proData.orderStatus = json_data.getString("order_status");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();
                            }


                            LinearLayoutManager layoutManager = new LinearLayoutManager(Orders.this);
                            adapterOrders = new AdapterOrders(Orders.this, data);
                            saleRecycle.setHasFixedSize(false);
                            saleRecycle.setAdapter(adapterOrders);
                            saleRecycle.setLayoutManager(layoutManager);
                            adapterOrders.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        saleRecycle.setVisibility(View.GONE);
                        Toast.makeText(Orders.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }
}
