package f.nexld.co.za.nextleveldistribution.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.ProductDetails;
import f.nexld.co.za.nextleveldistribution.R;
import f.nexld.co.za.nextleveldistribution.data.DataCartItems;
import f.nexld.co.za.nextleveldistribution.data.DataItems;

/**
 * Created by DEBONGZ on 4/8/2018.
 */

public class AdapterDetailItems extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataCartItems> data= Collections.emptyList();
    DataItems current;
    int currentPos=0;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterDetailItems(Context context, List<DataCartItems> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_detail_items, parent,false);
        AdapterDetailItems.MyHolder holder=new AdapterDetailItems.MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        final AdapterDetailItems.MyHolder myHolder= (AdapterDetailItems.MyHolder) holder;
        final DataCartItems current=data.get(position);

        myHolder.textItemName.setText(current.productName);
        myHolder.textItemPrice.setText("R"+ current.productPrice);
        myHolder.textQuantity.setText(current.productQuantity);
        //myHolder.texttopRoomID.setText(current.topRoomID);



        final String product_id = current.productId;
        final String product_name = current.productName;
        final String product_price = current.productPrice;

        myHolder.mainRaiderLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//
//                //Toast.makeText(context, "PRO: "+product_name, Toast.LENGTH_SHORT).show();
//
//                Intent intent= new Intent(context, ProductDetails.class);
//                intent.putExtra("product_id", product_id);
//                intent.putExtra("product_name", product_name);
//                intent.putExtra("product_price", product_price);
//                intent.putExtra("image_url", image_url);
//                context.startActivity(intent);

            }
        });

    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        TextView textItemName;
        TextView textItemPrice;
        TextView textQuantity;
        RelativeLayout mainRaiderLine;





        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            textItemName = (TextView) itemView.findViewById(R.id.item_name);
            textItemPrice= (TextView) itemView.findViewById(R.id.item_price);
            mainRaiderLine =(RelativeLayout)itemView.findViewById(R.id.mainraider_line);
            textQuantity = (TextView) itemView.findViewById(R.id.item_quantity);
            //texttopRoomID = (TextView) itemView.findViewById(R.id.toproomId);
        }


        @Override
        public void onClick(View v) {

        }
    }



}
