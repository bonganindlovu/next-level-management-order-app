package f.nexld.co.za.nextleveldistribution;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.adapters.AdapterProducts;
import f.nexld.co.za.nextleveldistribution.data.DataItems;

public class Products extends AppCompatActivity {
    RelativeLayout search_tab;
    RecyclerView saleRecycle;
    AdapterProducts adapterProducts;
    List<DataItems> data;
    Spinner cat;
    EditText search_products;
    NestedScrollView scroll;
    LinearLayoutManager layoutManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);
        search_tab = findViewById(R.id.search_tab);
        saleRecycle = (RecyclerView) findViewById(R.id.productsRecycle);
        scroll = (NestedScrollView)findViewById(R.id.scroll);
        saleRecycle.setNestedScrollingEnabled(false);
        saleRecycle.setHasFixedSize(true);
        saleRecycle.setItemViewCacheSize(20);
        saleRecycle.setDrawingCacheEnabled(true);
        saleRecycle.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);




        cat = findViewById(R.id.cat);
        search_products = findViewById(R.id.search_products);

        String the_cat = String.valueOf(cat.getSelectedItem());
        String keyword = search_products.getText().toString();

        loadItems(the_cat,keyword);

        search_products.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                //Updating Array Adapter ListView after typing inside EditText.
                String the_cat = String.valueOf(cat.getSelectedItem());
                String keyword = search_products.getText().toString();

               //sea
                loadItems(the_cat,keyword);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String the_cat = String.valueOf(cat.getSelectedItem());
                String keyword = search_products.getText().toString();
                //sea

                loadItems(the_cat,keyword);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String the_cat = String.valueOf(cat.getSelectedItem());
                String keyword = search_products.getText().toString();

                loadItems(the_cat,keyword);
            }
        });


        cat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                String the_cat = String.valueOf(cat.getSelectedItem());
                String keyword = search_products.getText().toString();

                loadItems(the_cat,keyword);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });



    }

    public void hideSearch(View view) {
        search_tab.setVisibility(View.GONE);
    }

    public void showSearch(View view) {
        search_tab.setVisibility(View.VISIBLE);
    }

    private void loadItems(final String department,final String keyword) {
        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.PRODUCTS+"?department="+department+"&keyword="+keyword,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Products.this, "LINK: "+result, Toast.LENGTH_SHORT).show();

                        try {
                            JSONArray jArray = new JSONArray(result);
                            data = new ArrayList<>();
                            for (int i = 0; i < jArray.length(); i++) {


                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataItems proData = new DataItems();

                                proData.productId = json_data.getString("product_id");
                                proData.productBarcode = json_data.getString("product_barcode");
                                proData.productName = json_data.getString("product_name");
                                proData.productPrice = json_data.getString("original_price");
                                proData.productSalePrice = json_data.getString("sale_price");
                                proData.productSaleStatus = json_data.getString("sale_status");
                                proData.productSaleExp = json_data.getString("sale_expiration");
                                proData.productImage = json_data.getString("product_image_url");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();


                            }
                            LinearLayoutManager layoutManager = new LinearLayoutManager(Products.this);
                            adapterProducts = new AdapterProducts(Products.this, data);
                            saleRecycle.setHasFixedSize(true);
                            saleRecycle.setAdapter(adapterProducts);
                            saleRecycle.setLayoutManager(layoutManager);
                            adapterProducts.notifyDataSetChanged();

                        } catch (JSONException e) {
                            //Toast.makeText(Products.this, "LINK: "+e, Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(Products.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void goBack(View view) {
        super.onBackPressed();
    }
}
