package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FinalCheckOut extends AppCompatActivity {
    TextView order_number, message;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_check_out);
        order_number= findViewById(R.id.order_number);
        message = findViewById(R.id.message);
        getOrderTotals("");
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void getOrderTotals(final String torder_number) {

        RequestQueue requestQueue = Volley.newRequestQueue(FinalCheckOut.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.GET_ORDER_TOTALS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String torder_number = jsonObject1.getString("order_number");
                    String torder_date = jsonObject1.getString("order_date");
                    String torder_total = jsonObject1.getString("order_total");
                    String torder_status = jsonObject1.getString("order_status");

                    String tpayment_method = jsonObject1.getString("payment_method");
                    String tdelivery_method = jsonObject1.getString("delivery_method");

                    String tstreet_number = jsonObject1.getString("street_number");
                    String tsuburb = jsonObject1.getString("suburb");
                    String tcity = jsonObject1.getString("city");
                    String tbusiness_name = jsonObject1.getString("business_name");
                    String tbuilding_name = jsonObject1.getString("building_name");
                    String tdelivery_total = jsonObject1.getString("delivery_total");
                    order_number.setText("ORDER#: "+torder_number);
                    if (tdelivery_method.equalsIgnoreCase("Collection")){

                            message.setText("Please don't forget to bring your bank card or cash when coming to collect your order");

                    }
                    else{
                        message.setText("Please don't forget to keep your bank card or cash with you when the driver comes to deliver your order");
                    }

                    //payment_method.setText(tpayment_method);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("order_number", torder_number);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }
}
