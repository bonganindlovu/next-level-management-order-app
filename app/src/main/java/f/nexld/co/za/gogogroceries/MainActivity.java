package f.nexld.co.za.nextleveldistribution;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.adapters.AdapterItems;
import f.nexld.co.za.nextleveldistribution.adapters.AdapterSaleItems;
import f.nexld.co.za.nextleveldistribution.data.DataItems;
import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

import static f.nexld.co.za.nextleveldistribution.R.id.drawer_layout;
import static f.nexld.co.za.nextleveldistribution.R.id.error_page;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_account;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_cart;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_help;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_home;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_orders;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_settings;
import static f.nexld.co.za.nextleveldistribution.R.id.nav_view;
import static f.nexld.co.za.nextleveldistribution.R.id.newitem_recyclerview;
import static f.nexld.co.za.nextleveldistribution.R.id.onsaleRecyclerview;

public class MainActivity extends AppCompatActivity {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    Toolbar toolbar;
    private AdapterItems adapterNew;
    private AdapterSaleItems adapterSale;
    private View navHeader;
    GridLayoutManager layoutManager;
    RelativeLayout errorPage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(drawer_layout);
        navigationView = (NavigationView) findViewById(nav_view);
        errorPage= (RelativeLayout)findViewById(error_page);
        setUpNavigationView();
        loadNewItems();
        loadSpecialItems();

    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {

                    case nav_home:

                        drawer.closeDrawers();
                        break;
                    case nav_cart:
                        Intent intent= new Intent(MainActivity.this, Cart.class);;
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case nav_orders:
                        String email = MyPreferences.getUserEmail(MainActivity.this);
                        if (email.length() == 0) {
                            //go to log in
                            intent= new Intent(MainActivity.this, Login.class);;
                            intent.putExtra("next_page", "orders");
                            startActivity(intent);
                        }
                        else{
                            //go to checkout
                            intent= new Intent(MainActivity.this, Orders.class);;
                            startActivity(intent);

                        }
                        drawer.closeDrawers();

                        break;
                    case  nav_account:
                        email = MyPreferences.getUserEmail(MainActivity.this);
                        if (email.length() == 0) {
                            //go to log in
                            intent= new Intent(MainActivity.this, Login.class);;
                            intent.putExtra("next_page", "account");
                            startActivity(intent);
                        }
                        else{
                            //go to checkout
                            intent= new Intent(MainActivity.this, Account.class);;
                            startActivity(intent);

                        }
                        drawer.closeDrawers();

                        break;

                    case nav_settings:
                        intent= new Intent(MainActivity.this, Settings.class);;
                        startActivity(intent);
                        drawer.closeDrawers();
                        break;
                    case nav_help:
//                        intent= new Intent(MainActivity.this, Help.class);;
//                        startActivity(intent);
                        sendEmail();
                        drawer.closeDrawers();
                        break;
                    default:

                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
        emailIntent.setData(Uri.parse("mailto:"+"bongani@nextld.co.za"));

        startActivity(Intent.createChooser(emailIntent,"Send email using"));


    }

    public void openMenu(View view) {
        drawer.openDrawer(GravityCompat.START);

    }

    private void loadNewItems() {

        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.HOME_PRODUCTS_NEW+"?product_type=new",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Products.this, "LINK: "+result, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jArray = new JSONArray(result);

                            List<DataItems> data = new ArrayList<>();

                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataItems proData = new DataItems();

                                proData.productId = json_data.getString("product_id");
                                proData.productBarcode = json_data.getString("product_barcode");
                                proData.productName = json_data.getString("product_name");
                                proData.productPrice = json_data.getString("original_price");
                                proData.productSalePrice = json_data.getString("sale_price");
                                proData.productSaleStatus = json_data.getString("sale_status");
                                proData.productSaleExp = json_data.getString("sale_expiration");
                                proData.productImage = json_data.getString("product_image_url");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();
                            }
                            LinearLayoutManager linearLayoutManager
                                    = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
                           RecyclerView newRecycle = (RecyclerView)findViewById(newitem_recyclerview);
                           adapterNew = new AdapterItems(MainActivity.this, data);
                            //RecycleViewTopRooms.setHasFixedSize(true);
                            newRecycle.setAdapter(adapterNew);
                            newRecycle.setLayoutManager(linearLayoutManager);
                            adapterNew.notifyDataSetChanged();
                        } catch (JSONException e) {
                            //Toast.makeText(MainActivity.this, ""+e, Toast.LENGTH_LONG).show();
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorPage.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this, "Error! Please check internet connection.\nOr click the image to retry", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }

    private void loadSpecialItems() {

        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.HOME_PRODUCTS_NEW+"?product_type=sale",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(MainActivity.this, "LINK: "+result, Toast.LENGTH_SHORT).show();
                        try {
                            JSONArray jArray = new JSONArray(result);


                            List<DataItems> data = new ArrayList<>();


                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataItems proData = new DataItems();

                                proData.productId = json_data.getString("product_id");
                                proData.productBarcode = json_data.getString("product_barcode");
                                proData.productName = json_data.getString("product_name");
                                proData.productPrice = json_data.getString("original_price");
                                proData.productSalePrice = json_data.getString("sale_price");
                                proData.productSaleStatus = json_data.getString("sale_status");
                                proData.productSaleExp = json_data.getString("sale_expiration");
                                proData.productImage = json_data.getString("product_image_url");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();
                            }

                            DisplayMetrics displayMetrics = MainActivity.this.getResources().getDisplayMetrics();
                            float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
                            float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
                            //Toast.makeText(getActivity(),""+dpWidth, Toast.LENGTH_LONG).show();

                            if(dpWidth <=360 ){

                                 layoutManager = new GridLayoutManager(getApplicationContext(),2);
                            }
                            else if(dpWidth>=600 &&  dpWidth<=720){

                                layoutManager = new GridLayoutManager(getApplicationContext(),3);
                            }
                            else if(dpWidth>=720){

                                layoutManager = new GridLayoutManager(getApplicationContext(),4);
                            }
                            else{
                                layoutManager = new GridLayoutManager(getApplicationContext(),2);
                            }
                            RecyclerView saleRecycle = (RecyclerView) findViewById(onsaleRecyclerview);
                            adapterSale = new AdapterSaleItems(MainActivity.this, data);
                            saleRecycle.setHasFixedSize(false);
                            saleRecycle.setAdapter(adapterSale);
                            saleRecycle.setLayoutManager(layoutManager);
                            adapterSale.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        errorPage.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this, "Error! Please check internet connection\nOr click the image to retry", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }

    public void retryConnect(View view) {
        errorPage.setVisibility(View.INVISIBLE);
        loadNewItems();
        loadSpecialItems();

    }

    public void openProducts(View view) {
        Intent intent= new Intent(MainActivity.this, Products.class);;
        startActivity(intent);
    }
}
