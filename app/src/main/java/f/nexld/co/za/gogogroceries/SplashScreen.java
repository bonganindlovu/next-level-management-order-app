package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import java.util.UUID;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.SHARED_PREF_NAME;
import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.TAG_ID;

public class SplashScreen extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideStatusBar();
        setContentView(R.layout.activity_splash_screen);

        final String device_id = MyPreferences.getDeviceID(this);
        if (device_id.length() == 0) {
            String new_id =UUID.randomUUID().toString();
            saveDeviceId(new_id);
        }
        else{

        }

        Thread thread = new Thread() {

            @Override
            public void run() {
                try {
                    sleep(3000);

                        Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                        startActivity(intent);
                        finish();


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        };

        thread.start();
    }

    public void hideStatusBar(){
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        else {
            View decorView = getWindow().getDecorView();
            // Hide Status Bar.
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }

    public void saveDeviceId(String id){

        SharedPreferences shared = getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(TAG_ID, id);
        editor.apply();
    }

    //this method will fetch the device token from shared preferences

}
