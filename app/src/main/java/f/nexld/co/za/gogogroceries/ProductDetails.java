package f.nexld.co.za.nextleveldistribution;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.HashMap;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;
import f.nexld.co.za.nextleveldistribution.data.VolleySingleton;

public class ProductDetails extends AppCompatActivity {
    TextView productName,productPrice;
    TextView textViewBottomSheetState,b_product_name,sheet_state;
    RelativeLayout toggleBottomSheet,overlay,sheet_options;
    BottomSheetBehavior bottomSheetBehavior;
    ConstraintLayout bottomSheetLayout;
    ImageView sheet_image,sheet_success,b_product_image,product_image;
    TextView sheet_tittle,item_price_special;
    EditText product_quantity;
    ProgressBar add_product_loader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        productName = (TextView)findViewById(R.id.product_name);
        productPrice = (TextView)findViewById(R.id.product_price);
        item_price_special = findViewById(R.id.item_price_special);

        add_product_loader = findViewById(R.id.add_product_loader);
        sheet_image = findViewById(R.id.sheet_image);
        sheet_success = findViewById(R.id.sheet_success);
        sheet_tittle = findViewById(R.id.sheet_tittle);
        sheet_options = findViewById(R.id.sheet_options);
        b_product_image = findViewById(R.id.b_product_image);
        product_image = findViewById(R.id.product_image);
        product_quantity = findViewById(R.id.product_quantity);

        // bind UI
        toggleBottomSheet = findViewById(R.id.footer);
        overlay = findViewById(R.id.overlay);
        bottomSheetLayout = findViewById(R.id.bottom_sheet);
        // init the bottom sheet behavior
        b_product_name = findViewById(R.id.b_product_name);
        sheet_state = findViewById(R.id.sheet_state);


        Intent in = getIntent();
        String product_id = in.getExtras().getString("product_id");
        String product_name = in.getExtras().getString("product_name");
        String product_price = in.getExtras().getString("product_price");
        String sale_price = in.getExtras().getString("sale_price");
        String sale_status = in.getExtras().getString("sale_status");
        String image_url = in.getExtras().getString("image_url");

        if (sale_status.equalsIgnoreCase("sale_on")){
            item_price_special.setText(product_price);
            productPrice.setText(sale_price);
            item_price_special.setVisibility(View.VISIBLE);
        }
        else{
            productPrice.setText(product_price);
            item_price_special.setVisibility(View.GONE);
        }

        productName.setText(product_name);
        b_product_name.setText(product_name);
        item_price_special.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        Glide.with(ProductDetails.this)
                .load(image_url)
                .thumbnail(0.1f)
                .animate(R.anim.abc_fade_in)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .centerCrop()
                .into(b_product_image);

        Glide.with(ProductDetails.this)
                .load(image_url)
                .thumbnail(0.1f)
                .animate(R.anim.abc_fade_in)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .centerCrop()
                .into(product_image);






        // set listener on button click
        toggleBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
               overlay.setVisibility(View.VISIBLE);
               bottomSheetLayout.setVisibility(View.VISIBLE);
                sheet_state.setText("showing");
            }
        });

        overlay.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                overlay.setVisibility(View.GONE);
                bottomSheetLayout.setVisibility(View.GONE);
                sheet_state.setText("not_showing");
            }
        });



    }

    public void goBack(View view) {
        String the_state = sheet_state.getText().toString();
        if (the_state.equalsIgnoreCase("showing")){
            overlay.setVisibility(View.GONE);
            bottomSheetLayout.setVisibility(View.GONE);
            sheet_state.setText("not_showing");
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        String the_state = sheet_state.getText().toString();
        if (the_state.equalsIgnoreCase("showing")){
            overlay.setVisibility(View.GONE);
            bottomSheetLayout.setVisibility(View.GONE);
            sheet_state.setText("not_showing");
        }
        else{
            super.onBackPressed();
        }

    }

    public  void showBtmSuccess(){
        sheet_image.setVisibility(View.GONE);
        sheet_success.setVisibility(View.VISIBLE);
        sheet_options.setVisibility(View.VISIBLE);
        sheet_tittle.setText("ITEM ADDED TO CART");
        sheet_tittle.setTextColor(Color.BLACK);
    }

    public void triggerAdd(View view) {
        //get item details and quantity
        String item_quantity = product_quantity.getText().toString();
        int qua = Integer.parseInt(item_quantity);
        Intent in = getIntent();
        String product_id = in.getExtras().getString("product_id");
        String product_name = in.getExtras().getString("product_name");
        String product_price = in.getExtras().getString("product_price");
        String sale_price = in.getExtras().getString("sale_price");
        String sale_status = in.getExtras().getString("sale_status");

        if (sale_status.equalsIgnoreCase("sale_off")){
            product_price = in.getExtras().getString("product_price");
        }
        else {
            product_price = in.getExtras().getString("sale_price");
        }


        String device_id = MyPreferences.getDeviceID(this);

        //Toast.makeText(ProductDetails.this,""+item_quantity, Toast.LENGTH_LONG).show();
        if (qua<1){
            Toast.makeText(ProductDetails.this,"Quantity needs to be more tha 1", Toast.LENGTH_LONG).show();

        }
        else if(item_quantity.isEmpty() || item_quantity.equals(null)){
            Toast.makeText(ProductDetails.this,"Quantity needs to be more tha 1", Toast.LENGTH_LONG).show();
        }
        else{
            addToCart(product_id,product_name,item_quantity,product_price,device_id);
        }



    }

    public void gotoProducts(View view) {
            overlay.setVisibility(View.GONE);
            bottomSheetLayout.setVisibility(View.GONE);
            sheet_state.setText("not_showing");
            super.onBackPressed();

    }

    public void gotoCart(View view) {
        overlay.setVisibility(View.GONE);
        bottomSheetLayout.setVisibility(View.GONE);
        sheet_state.setText("not_showing");
        Intent intent= new Intent(ProductDetails.this, Cart.class);;
        startActivity(intent);
        finish();
    }

    //check first if the item is existing on the cart
    //if the item exist, show the existing quantity and
    //if not show default
    //

    public void addToCart(final String p_id,final String p_name,final String p_quantity,final String p_price,final String d_id) {
        add_product_loader.setVisibility(View.VISIBLE);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.ADD_TO_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        add_product_loader.setVisibility(View.GONE);
                        if (response.equalsIgnoreCase("done")){
                            showBtmSuccess();
                        }
                        else{
                            Toast.makeText(ProductDetails.this,"Server error ! Adding product failed", Toast.LENGTH_LONG).show();

                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        add_product_loader.setVisibility(View.GONE);
                        // toast network error

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("product_id", p_id);
                params.put("product_name", p_name);
                params.put("item_quantity", p_quantity);
                params.put("product_price", p_price);
                params.put("device_id", d_id);
                return params;
            }
        };
        VolleySingleton.getInstance(ProductDetails.this).addToRequestQueue(stringRequest);
    }
}
