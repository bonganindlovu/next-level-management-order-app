package f.nexld.co.za.nextleveldistribution;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.adapters.AdapterCartProducts;
import f.nexld.co.za.nextleveldistribution.data.DataCartItems;
import f.nexld.co.za.nextleveldistribution.data.MyPreferences;
import f.nexld.co.za.nextleveldistribution.data.VolleySingleton;

public class Cart extends AppCompatActivity {
    private AdapterCartProducts adapterCart;
    TextView  total_number,total_amount,check_out,error;
    RecyclerView saleRecycle;
    RelativeLayout top_cart;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        total_amount = findViewById(R.id.total_amount);
        total_number = findViewById(R.id.total_number);
        check_out = findViewById(R.id.check_out);
        error=findViewById(R.id.error);
        saleRecycle = (RecyclerView) findViewById(R.id.cartRecycle);
        top_cart = findViewById(R.id.top_cart);
        dialog = new ProgressDialog(this);
        String device_id = MyPreferences.getDeviceID(this);
        getCartTotals(device_id);
        loadCartItems(device_id);

    }


    private void loadCartItems(final String device_id) {

        RequestQueue mmrequestQueuecha = Volley.newRequestQueue(this);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, EndPoints.LOAD_CART+"?device_id="+device_id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String result) {
                        //Toast.makeText(Cart.this, "LINK: "+result, Toast.LENGTH_SHORT).show();
                        if (result.equalsIgnoreCase("no_results")){
                            check_out.setVisibility(View.GONE);
                            error.setVisibility(View.VISIBLE);
                            saleRecycle.setVisibility(View.GONE);
                            top_cart.setVisibility(View.GONE);

                        }
                        else{
                            check_out.setVisibility(View.VISIBLE);
                            saleRecycle.setVisibility(View.VISIBLE);
                            error.setVisibility(View.GONE);
                            top_cart.setVisibility(View.VISIBLE);

                        }
                        try {
                            JSONArray jArray = new JSONArray(result);


                            List<DataCartItems> data = new ArrayList<>();


                            for (int i = 0; i < jArray.length(); i++) {
                                JSONObject d = jArray.getJSONObject(i);
                                JSONObject json_data = jArray.getJSONObject(i);
                                DataCartItems proData = new DataCartItems();

                                proData.productId = json_data.getString("product_id");
                                proData.productName = json_data.getString("product_name");
                                proData.productPrice = json_data.getString("original_price");
                                proData.productImage = json_data.getString("product_image_url");
                                proData.productQuantity = json_data.getString("product_quantity");
                                data.add(proData);
                                //zmAdaptera.notifyDataSetChanged();
                            }


                            LinearLayoutManager layoutManager = new LinearLayoutManager(Cart.this);
                            adapterCart = new AdapterCartProducts(Cart.this, data);
                            saleRecycle.setHasFixedSize(false);
                            saleRecycle.setAdapter(adapterCart);
                            saleRecycle.setLayoutManager(layoutManager);
                            adapterCart.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        check_out.setVisibility(View.GONE);
                        saleRecycle.setVisibility(View.GONE);
                        top_cart.setVisibility(View.GONE);
                        Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                    }
                }) {

        };

        MyVolley.getInstance(this).addToRequestQueue(stringRequest);

    }
    public void goBack(View view) {
        super.onBackPressed();
    }


    public void getCartTotals(final String device_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(Cart.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_CART_TOTALS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String total_price = jsonObject1.getString("total_price");
                    String total_items = jsonObject1.getString("total_items");
                    total_number.setText("( "+total_items+" )");
                    total_amount.setText("R "+total_price);





                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("device_id", device_id);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }

    public void checkOut(View view) {


        String email = MyPreferences.getUserEmail(this);
        String address = MyPreferences.getAddress(this);

        if (email.length() == 0) {
            //go to log in
            Intent intent= new Intent(Cart.this, Login.class);;
            intent.putExtra("next_page", "check");
            startActivity(intent);
        }
        else if (address.length()==0){
            Intent intent= new Intent(Cart.this, AddAddress.class);;
            intent.putExtra("next_page", "check");
            startActivity(intent);
        }

        else{
            //go to checkout
            Intent intent= new Intent(Cart.this, Checkout.class);;
            startActivity(intent);
        }
    }

    @Override
    protected void onResume() {
        String device_id = MyPreferences.getDeviceID(this);
        getCartTotals(device_id);
        loadCartItems(device_id);
        super.onResume();
    }
    public void showEdit(final  String product_id, final String product_name,final String product_price,final String image_url, final String product_quantity){
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(Cart.this);
        View mView = layoutInflaterAndroid.inflate(R.layout.bottom_sheet_two, null);
        final AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Cart.this);
        alertDialogBuilderUserInput.setView(mView);
        TextView pro_id;
        TextView pro_name,update_item,remove_item;
        final EditText pro_quantity;
        ImageView img_url;

        pro_id = (TextView)mView.findViewById(R.id.product_id);
        update_item = (TextView)mView.findViewById(R.id.update_item);
        remove_item = (TextView)mView.findViewById(R.id.remove_item);
        pro_name = (TextView)mView.findViewById(R.id.b_product_name);
        pro_quantity = (EditText)mView.findViewById(R.id.b_product_quantity);
        img_url = (ImageView)mView.findViewById(R.id.b_product_image);


        pro_id.setText(product_id);
        pro_name.setText(product_name);
        pro_quantity.setText(product_quantity);


        Glide.with(this)
                .load(image_url)
                .thumbnail(0.1f)
                .animate(R.anim.abc_fade_in)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .centerCrop()
                .into(img_url);

        final String device_id = MyPreferences.getDeviceID(Cart.this);
        update_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String item_q = pro_quantity.getText().toString();
                int qua = Integer.parseInt(item_q);

                //Toast.makeText(Cart.this, "image:"+item_q, Toast.LENGTH_SHORT).show();


               // Toast.makeText(ProductDetails.this,""+item_quantity, Toast.LENGTH_LONG).show();
                if (qua<1){
                    Toast.makeText(Cart.this,"Quantity needs to be more tha 1", Toast.LENGTH_LONG).show();

                }
                else if(item_q.isEmpty() || item_q.equals(null)){
                    Toast.makeText(Cart.this,"Quantity needs to be more tha 1", Toast.LENGTH_LONG).show();
                }
                else{
                    addToCart(product_id,product_name,item_q,product_price,device_id);
                }




            }
        });
        remove_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

             removeItem(product_id,device_id);
            }
        });


        alertDialogBuilderUserInput
                .setCancelable(true);

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();



    }
    public void addToCart(final String p_id,final String p_name,final String p_quantity,final String p_price,final String d_id) {
        //add_product_loader.setVisibility(View.VISIBLE);
        dialog.setMessage("updating item...");
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.ADD_TO_CART,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //add_product_loader.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (response.equalsIgnoreCase("done")){
                          //  showBtmSuccess();
                            Toast.makeText(Cart.this,"Item updated!", Toast.LENGTH_LONG).show();
                            Intent intent= new Intent(Cart.this, Cart.class);;
                            startActivity(intent);
                            finish();

                        }
                        else{
                            Toast.makeText(Cart.this,"Server error ! Adding product failed", Toast.LENGTH_LONG).show();

                            Intent intent= new Intent(Cart.this, Cart.class);;
                            startActivity(intent);
                            finish();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Intent intent= new Intent(Cart.this, Cart.class);;
                        startActivity(intent);
                        finish();
                        //add_product_loader.setVisibility(View.GONE);
                        // toast network error

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("product_id", p_id);
                params.put("product_name", p_name);
                params.put("item_quantity", p_quantity);
                params.put("product_price", p_price);
                params.put("device_id", d_id);
                return params;
            }
        };
        VolleySingleton.getInstance(Cart.this).addToRequestQueue(stringRequest);
    }
    public void triggerAdd(View view) {
    }

    public void removeItem(final String p_id,final String d_id) {
        //add_product_loader.setVisibility(View.VISIBLE);
        dialog.setMessage("removing item...");
        dialog.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.REMOVE_ITEM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //add_product_loader.setVisibility(View.GONE);
                        dialog.dismiss();
                        if (response.equalsIgnoreCase("done")){
                            //  showBtmSuccess();
                            Toast.makeText(Cart.this,"Item removed!", Toast.LENGTH_LONG).show();
                            Intent intent= new Intent(Cart.this, Cart.class);;
                            startActivity(intent);
                            finish();

                        }
                        else{
                            Toast.makeText(Cart.this,"Server error ! Adding product failed", Toast.LENGTH_LONG).show();

                            Intent intent= new Intent(Cart.this, Cart.class);;
                            startActivity(intent);
                            finish();
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        dialog.dismiss();
                        Intent intent= new Intent(Cart.this, Cart.class);;
                        startActivity(intent);
                        finish();
                        //add_product_loader.setVisibility(View.GONE);
                        // toast network error

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("product_id", p_id);
                params.put("device_id", d_id);
                return params;
            }
        };
        VolleySingleton.getInstance(Cart.this).addToRequestQueue(stringRequest);
    }
}
