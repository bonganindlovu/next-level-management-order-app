package f.nexld.co.za.nextleveldistribution;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

public class EditAccount extends AppCompatActivity {
    private ProgressDialog progressDialog;
    EditText first_name,surname,user_cell,user_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account);
        first_name = findViewById(R.id.first_name);
        surname = findViewById(R.id.surname);
        user_cell = findViewById(R.id.user_cell);
        user_email = findViewById(R.id.user_email);


        String email = MyPreferences.getUserEmail(this);
        getUserDetails(email);

    }

    public void updateProfile(View view) {

        String firstname=first_name.getText().toString();
        String lastname = surname.getText().toString();
        String email = user_email.getText().toString();
        String phone = user_cell.getText().toString();
        if (firstname.length()<4){
            Toast.makeText(EditAccount.this, "Invalid First name!", Toast.LENGTH_SHORT).show();
        }
        else if (lastname.length()<3){
            Toast.makeText(EditAccount.this, "Invalid Surname!", Toast.LENGTH_SHORT).show();
        }
        else if (phone.length()<7){
            Toast.makeText(EditAccount.this, "Invalid phone number!", Toast.LENGTH_SHORT).show();
        }
        else{
            //continue

            updateProfileInfo(firstname,lastname,email,phone);
        }
    }

    private void updateProfileInfo(final String firstname,final String lastname,final String email,final String phone) {
        RequestQueue requestQueue = Volley.newRequestQueue(EditAccount.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.UPDATE_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Toast.makeText(EditAccount.this, "Profile Updated!", Toast.LENGTH_SHORT).show();

                        Intent in = getIntent();
                        String next_page = in.getExtras().getString("next_page");

                        //go to check
                        Intent intent= new Intent(EditAccount.this, Account.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("next_page", next_page);
                        startActivity(intent);
                        finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(EditAccount.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("name", firstname);
                stringMap.put("surname", lastname);
                stringMap.put("email", email);
                stringMap.put("phone", phone);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(EditAccount.this);
        progressDialog.setMessage("Updating...");
        progressDialog.show();

    }


    public void getUserDetails(final String user_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(EditAccount.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);

                    String tuser_first_name = jsonObject1.getString("firstname");
                    String tuser_last_name = jsonObject1.getString("surname");
                    String tuser_cell = jsonObject1.getString("mobile");

                    first_name.setText(tuser_first_name);
                    surname.setText(tuser_last_name);
                    user_cell.setText(tuser_cell);
                    user_email.setText(user_id);


                } catch (JSONException e) {
                    //Toast.makeText(Account.this, ""+e, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("user_id", user_id);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }
}
