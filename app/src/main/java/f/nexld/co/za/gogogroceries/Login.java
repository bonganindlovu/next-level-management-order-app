package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.saveDeviceAddress;
import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.saveDeviceEmail;

public class Login extends AppCompatActivity {
    EditText user_email, password,email_txt;
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



        user_email=findViewById(R.id.user_email);
        password=findViewById(R.id.password);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void register(View view) {
        Intent in = getIntent();
        String next_page = in.getExtras().getString("next_page");

        Intent intent= new Intent(Login.this, Register.class);;
        intent.putExtra("next_page", next_page);
        startActivity(intent);
        finish();
    }

    public void login(final String email,final String user_password) {

        RequestQueue requestQueue = Volley.newRequestQueue(Login.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOG_IN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String status = jsonObject1.getString("status");
                    String user_email = jsonObject1.getString("user_email");
                    String message = jsonObject1.getString("message");
                    String add = jsonObject1.getString("add");

                    if (status.equalsIgnoreCase("passed")) {
                        Toast.makeText(Login.this, "" + message, Toast.LENGTH_SHORT).show();
                        saveDeviceEmail(Login.this,user_email);
                        if (add.equalsIgnoreCase("save")){
                            saveDeviceAddress(Login.this,"saved");
                        }
                        else{

                        }
                        Intent in = getIntent();
                        String next_page = in.getExtras().getString("next_page");
                        //go to the next page
                        if (next_page.equalsIgnoreCase("check")){
                            //go to check
                            Intent intent= new Intent(Login.this, Checkout.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        else if (next_page.equalsIgnoreCase("orders")){
                            //go to orders
                            Intent intent= new Intent(Login.this, Orders.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        else if (next_page.equalsIgnoreCase("account")){
                            //go to account
                            Intent intent= new Intent(Login.this, Account.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                        else{
                            //go to main
                            Intent intent= new Intent(Login.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }

                    }
                    else{
                        Toast.makeText(Login.this, "" + message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(Login.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("email", email);
                stringMap.put("user_password", user_password);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setMessage("Signing in...");
        progressDialog.show();

    }

    public void logIn(View view) {
        String email = user_email.getText().toString().trim();
        String user_password= password.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        if (email.matches(emailPattern) || email.length() >1){

            if (user_password.isEmpty() || user_password.length() <1){
                Toast.makeText(Login.this, "Password is required!", Toast.LENGTH_SHORT).show();

            }

            else{
                login(email,user_password);
            }


        }
        else{
            Toast.makeText(Login.this, "Invalid Email!", Toast.LENGTH_SHORT).show();
        }


    }
    public void resetPassword(View view) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.reset_pass_dialog_box, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(Login.this);
        email_txt = (EditText)mView.findViewById(R.id.reset_email);

        alertDialogBuilderUserInput.setView(mView);


        alertDialogBuilderUserInput
                .setCancelable(false)
                .setPositiveButton("UPDATE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogBox, int id) {
                        String emaitext = email_txt.getText().toString();

                        resetPassword(emaitext);

                    }
                })
                .setNegativeButton("CANCEL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.show();
    }

    private void resetPassword(final String email) {
        RequestQueue requestQueue = Volley.newRequestQueue(Login.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.RESET_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Toast.makeText(Login.this, "Email Sent!", Toast.LENGTH_SHORT).show();

                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(Login.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("email", email);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(Login.this);
        progressDialog.setMessage("Resetting Account...");
        progressDialog.show();

    }
}
