package f.nexld.co.za.nextleveldistribution.data;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by DEBONGZ on 3/25/2018.
 */

public class MyPreferences {
    public static final String SHARED_PREF_NAME = "FCMSharedPref";
    public static final String TAG_ID = "device_id";
    public static final String TAG_EMAIL = "email";
    public static final String TAG_ADD = "add";

    private static Context mCtx;




    //this method will save the device token to shared preferences

    public static String getDeviceID(Context context){
        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        final String the_id= shared.getString(TAG_ID, "");
        return  the_id ;
    }

    public static String getUserEmail(Context context){
        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        final String the_id= shared.getString(TAG_EMAIL, "");
        return  the_id ;
    }
    public static String getAddress(Context context){
        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        final String the_id= shared.getString(TAG_ADD, "");
        return  the_id ;
    }

    public static void saveDeviceEmail(Context context, final String email){

        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(TAG_EMAIL, email);
        editor.apply();
    }

    public static void saveDeviceAddress(Context context, final String add){

        SharedPreferences shared = context.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(TAG_ADD, add);
        editor.apply();
    }
}
