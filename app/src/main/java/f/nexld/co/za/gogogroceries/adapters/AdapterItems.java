package f.nexld.co.za.nextleveldistribution.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.Collections;
import java.util.List;

import f.nexld.co.za.nextleveldistribution.ProductDetails;
import f.nexld.co.za.nextleveldistribution.R;
import f.nexld.co.za.nextleveldistribution.data.DataItems;

/**
 * Created by DEBONGZ on 4/8/2018.
 */

public class AdapterItems extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<DataItems> data= Collections.emptyList();
    DataItems current;
    int currentPos=0;


    // create constructor to initialize context and data sent from MainActivity
    public AdapterItems(Context context, List<DataItems> data){
        this.context=context;
        inflater= LayoutInflater.from(context);
        this.data=data;
    }
    // Inflate the layout when ViewHolder created
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.container_new_items, parent,false);
        AdapterItems.MyHolder holder=new AdapterItems.MyHolder(view);
        return holder;
    }
    // Bind data
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        // Get current position of item in RecyclerView to bind data and assign values from list
        AdapterItems.MyHolder myHolder= (AdapterItems.MyHolder) holder;
        DataItems current=data.get(position);

        myHolder.textItemName.setText(current.productName);
        final  String sale_status = current.productSaleStatus;
        if (sale_status.equalsIgnoreCase("sale_on")){
            myHolder.textItemPrice.setText("R"+ current.productSalePrice);
            myHolder.textitem_price_special.setText(current.productPrice);
            myHolder.textitem_price_special.setVisibility(View.VISIBLE);
            myHolder.textitem_price_special.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else{
            myHolder.textitem_price_special.setVisibility(View.GONE);
            myHolder.textItemPrice.setText("R"+ current.productPrice);
        }
        //myHolder.texttopRoomID.setText(current.topRoomID);
        final  String image_url = current.productImage;


        Glide.with(context)
                .load(image_url)
                .thumbnail(0.1f)
                .animate(R.anim.abc_fade_in)
                .placeholder(R.drawable.image_placeholder)
                .error(R.drawable.image_placeholder)
                .centerCrop()
                .into(myHolder.itemImage);

        final String product_id = current.productId;
        final String product_name = current.productName;
        final String product_price = current.productPrice;
        final String sale_price = current.productSalePrice;

        myHolder.mainRaiderLine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Toast.makeText(context, "PRO: "+product_name, Toast.LENGTH_SHORT).show();

                Intent intent= new Intent(context, ProductDetails.class);
                intent.putExtra("product_id", product_id);
                intent.putExtra("product_name", product_name);
                intent.putExtra("product_price", product_price);
                intent.putExtra("sale_price", sale_price);
                intent.putExtra("sale_status", sale_status);
                intent.putExtra("image_url", image_url);
                context.startActivity(intent);

            }
        });


    }
    // return total item from List
    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyHolder extends RecyclerView.ViewHolder  implements View.OnClickListener{

        TextView textItemName;
        TextView textItemPrice;
        TextView textItemId;
        TextView textitem_price_special;
        RelativeLayout mainRaiderLine;
        ImageView itemImage;





        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            textItemName = (TextView) itemView.findViewById(R.id.item_name);
            textItemPrice= (TextView) itemView.findViewById(R.id.item_price);
            mainRaiderLine =(RelativeLayout)itemView.findViewById(R.id.mainraider_line);
            itemImage = (ImageView) itemView.findViewById(R.id.newitem_image);
            textitem_price_special = (TextView) itemView.findViewById(R.id.item_price_special);
        }


        @Override
        public void onClick(View v) {

        }
    }

}
