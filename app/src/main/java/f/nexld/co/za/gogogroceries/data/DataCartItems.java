package f.nexld.co.za.nextleveldistribution.data;

/**
 * Created by DEBONGZ on 3/25/2018.
 */

public class DataCartItems {
    public String productId;
    public String productBarcode;
    public String productName;
    public String productPrice;
    public String productSalePrice;
    public String productSaleStatus;
    public String productSaleExp;
    public String productImage;
    public String productQuantity;

}
