package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

public class Checkout extends AppCompatActivity {
    private ProgressDialog progressDialog;
    TextView total_items,total_item_cost,delivery_amount,address,total_to_pay,delivery_method
            ,suburb,city,business_name,building_name,payment_method,minimum_total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        total_to_pay = findViewById(R.id.total_to_pay);
        address = findViewById(R.id.address);
        delivery_amount = findViewById(R.id.delivery_amount);
        total_item_cost=findViewById(R.id.total_item_cost);
        total_items=findViewById(R.id.total_items);
        delivery_method=findViewById(R.id.delivery_method);
        suburb=findViewById(R.id.suburb);
        city = findViewById(R.id.city);
        business_name = findViewById(R.id.business_name);
        building_name = findViewById(R.id.building_name);
        payment_method = findViewById(R.id.payment_method);
        minimum_total = findViewById(R.id.minimum_total);


        String device_id = MyPreferences.getDeviceID(this);

        getCartTotals(device_id);



    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void confirmOrder(View view) {
        String del_method = delivery_method.getText().toString();
        String device_id = MyPreferences.getDeviceID(this);
        String email = MyPreferences.getUserEmail(this);




        sendOrder(device_id,email,del_method);
    }

    public void getCartTotals(final String device_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(Checkout.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_CHECK_TOTALS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String total_price_amount = jsonObject1.getString("total_price");
                    String total_items_amount = jsonObject1.getString("total_items");
                    String delivery = jsonObject1.getString("delivery");
                    String total_invoice = jsonObject1.getString("total_invoice");
                    String payment_meth = jsonObject1.getString("payment_method");
                    String delivery_addr = jsonObject1.getString("delivery_address");
                    String delivery_suburb = jsonObject1.getString("delivery_suburb");
                    String delivery_city = jsonObject1.getString("delivery_city");
                    String bus_name = jsonObject1.getString("business_name");
                    String build_name = jsonObject1.getString("building_name");

                    String del_meth = jsonObject1.getString("delivery_method");
                    String min_tot = jsonObject1.getString("minimum_total");

                    double total_price_amount2 = Double.parseDouble(total_price_amount);
                    double min_tot2 = Double.parseDouble(min_tot);
                    if (del_meth.equalsIgnoreCase("Collection")){
                        minimum_total.setVisibility(View.GONE);

                    }
                    else  if (total_price_amount2> min_tot2){
                        minimum_total.setVisibility(View.GONE);
                    }
                    else{
                        minimum_total.setVisibility(View.VISIBLE);
                        minimum_total.setText("Order more than R"+min_tot+" and get free delivery for your order");
                    }


                    total_items.setText(total_items_amount);
                    total_item_cost.setText("R"+total_price_amount);
                    total_to_pay.setText("R"+total_invoice);
                    delivery_amount.setText(delivery);
                    payment_method.setText(payment_meth);
                    delivery_method.setText(del_meth);
                    address.setText(delivery_addr);
                    suburb.setText(delivery_suburb);
                    city.setText(delivery_city);
                    business_name.setText(bus_name);
                    building_name.setText(build_name);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                String email = MyPreferences.getUserEmail(Checkout.this);
                stringMap.put("user_id", email);
                stringMap.put("device_id", device_id);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }

    public void sendOrder(final String device_id, final String user_id, final String del_method) {

        RequestQueue requestQueue = Volley.newRequestQueue(Checkout.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.SEND_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();

                Intent intent= new Intent(Checkout.this, FinalCheckOut.class);;
                startActivity(intent);
                finish();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(Checkout.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                String delivery_fee = delivery_amount.getText().toString();
                stringMap.put("device_id", device_id);
                stringMap.put("user_id", user_id);
                stringMap.put("del_method", del_method);
                stringMap.put("delivery_fee", delivery_fee);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(Checkout.this);
        progressDialog.setMessage("Confirming order...");
        progressDialog.show();

    }

    public void addAdress(View view) {
        Intent intent= new Intent(Checkout.this, AddAddress.class);;
        intent.putExtra("next_page", "check");
        startActivity(intent);
        finish();
    }
}
