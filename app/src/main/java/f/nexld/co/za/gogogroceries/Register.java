package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static f.nexld.co.za.nextleveldistribution.data.MyPreferences.saveDeviceEmail;

public class Register extends AppCompatActivity {
    private ProgressDialog progressDialog;
    EditText first_name,surname,user_email,user_cell,password,password2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        first_name = findViewById(R.id.first_name);
        surname = findViewById(R.id.surname);
        user_email = findViewById(R.id.user_email);
        user_cell= findViewById(R.id.user_cell);
        password=findViewById(R.id.password);
        password2= findViewById(R.id.password2);
    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void logIn(View view) {
        Intent in = getIntent();
        String next_page = in.getExtras().getString("next_page");
        Intent intent= new Intent(Register.this, Login.class);;
        intent.putExtra("next_page", next_page);
        startActivity(intent);
        finish();
    }


    public void register(View view) {
        String firstname=first_name.getText().toString();
        String lastname = surname.getText().toString();
        String email = user_email.getText().toString();
        String phone = user_cell.getText().toString();
        String pass = password.getText().toString();
        String pass2 = password2.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";



        if (pass.equalsIgnoreCase(pass2)){
            //check the email
            if (email.matches(emailPattern) || email.length() >1){
                //check the rest
                if (firstname.length()<4){
                    Toast.makeText(Register.this, "Invalid First name!", Toast.LENGTH_SHORT).show();
                }
                else if (lastname.length()<3){
                    Toast.makeText(Register.this, "Invalid Surname!", Toast.LENGTH_SHORT).show();
                }
                else if (phone.length()<7){
                    Toast.makeText(Register.this, "Invalid phone number!", Toast.LENGTH_SHORT).show();
                }
                else if (pass.length()<4){
                    Toast.makeText(Register.this, "Password must be 4 characters or more!", Toast.LENGTH_SHORT).show();
                }
                else{
                    //continue

                    register(firstname,lastname,email,phone,pass);
                }


            }
            else{
                Toast.makeText(Register.this, "Invalid Email!", Toast.LENGTH_SHORT).show();
            }


        }
        else{
          //passwords do not match
            Toast.makeText(Register.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
        }





    }



    public void register(final String name,final String surname,final String email,final String phone,final String pass) {

        RequestQueue requestQueue = Volley.newRequestQueue(Register.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.REGISTER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                try {
                    //Toast.makeText(Cart.this, ""+response, Toast.LENGTH_SHORT).show();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String status = jsonObject1.getString("status");
                    String user_email = jsonObject1.getString("user_email");
                    String message = jsonObject1.getString("message");

                    if (status.equalsIgnoreCase("passed")) {
                        Toast.makeText(Register.this, "" + message, Toast.LENGTH_SHORT).show();
                        saveDeviceEmail(Register.this,user_email);
                        //go to the next page
                        Intent in = getIntent();
                        String next_page = in.getExtras().getString("next_page");

                            //go to check
                            Intent intent= new Intent(Register.this, AddAddress.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra("next_page", next_page);
                            startActivity(intent);
                            finish();
                            finish();

                    }
                    else{
                        Toast.makeText(Register.this, "" + message, Toast.LENGTH_SHORT).show();
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.i("Hitesh", "" + error);
                Toast.makeText(Register.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("name", name);
                stringMap.put("surname", surname);
                stringMap.put("email", email);
                stringMap.put("phone", phone);
                stringMap.put("pass", pass);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        progressDialog = new ProgressDialog(Register.this);
        progressDialog.setMessage("Signing up...");
        progressDialog.show();

    }
}
