package f.nexld.co.za.nextleveldistribution;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import f.nexld.co.za.nextleveldistribution.data.MyPreferences;

public class Account extends AppCompatActivity {
     TextView user_name,user_email,user_cell,business_name,building_name,
             street_number,suburb,city,delivery_method,payment_method;
     NestedScrollView delivery_city;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        user_name = findViewById(R.id.tuser_name);
        user_email = findViewById(R.id.tuser_email);
        user_cell = findViewById(R.id.tuser_cell);
        business_name = findViewById(R.id.tbusiness_name);
        building_name = findViewById(R.id.tbuilding_name);
        street_number = findViewById(R.id.tstreet_number);
        suburb = findViewById(R.id.tsuburb);
        city = findViewById(R.id.tcity);
        delivery_method = findViewById(R.id.tdelivery_method);
        payment_method = findViewById(R.id.tpayment_method);

        delivery_city = findViewById(R.id.nest);
        String email = MyPreferences.getUserEmail(this);
        getUserDetails(email);

    }

    public void goBack(View view) {
        super.onBackPressed();
    }

    public void getUserDetails(final String user_id) {

        RequestQueue requestQueue = Volley.newRequestQueue(Account.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, EndPoints.LOAD_PROFILE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //myrefresh.setRefreshing(false);
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("result");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);


                    String tpayment_method = jsonObject1.getString("payment_method");
                    String tdelivery_method = jsonObject1.getString("delivery_method");

                    String tuser_first_name = jsonObject1.getString("firstname");
                    String tuser_last_name = jsonObject1.getString("surname");
                    String tuser_cell = jsonObject1.getString("mobile");

                    String tstreet_number = jsonObject1.getString("delivery_address");
                    String tsuburb = jsonObject1.getString("delivery_suburb");
                    String tcity = jsonObject1.getString("delivery_city");
                    String tbusiness_name = jsonObject1.getString("business_name");
                    String tbuilding_name = jsonObject1.getString("building_name");

                    //Toast.makeText(Account.this, ""+tstreet_number, Toast.LENGTH_SHORT).show();
                    delivery_city.setVisibility(View.VISIBLE);
                    user_name.setText(tuser_first_name+" "+tuser_last_name);
                    user_cell.setText(tuser_cell);
                    user_email.setText(user_id);
                    payment_method.setText(tpayment_method);
                    delivery_method.setText(tdelivery_method);
                    street_number.setText(tstreet_number);
                    suburb.setText(tsuburb);
                    city.setText(tcity);
                    business_name.setText(tbusiness_name);
                    building_name.setText(tbuilding_name);


                } catch (JSONException e) {
                    //Toast.makeText(Account.this, ""+e, Toast.LENGTH_SHORT).show();
                    delivery_city.setVisibility(View.GONE);
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                delivery_city.setVisibility(View.GONE);
                Log.i("Hitesh", "" + error);
                //Toast.makeText(Cart.this, "Error! Please check internet connection", Toast.LENGTH_SHORT).show();
                //myrefresh.setRefreshing(false);
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> stringMap = new HashMap<>();
                stringMap.put("user_id", user_id);
                return stringMap;
            }

        };

        requestQueue.add(stringRequest);
        //initialize the progress dialog and show it
        //myrefresh.setRefreshing(true);

    }

    public void logOut(View view) {
        String MSHARED_PREF_NAME = MyPreferences.SHARED_PREF_NAME;
        SharedPreferences settings = getSharedPreferences(MSHARED_PREF_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().apply();


        Toast.makeText(Account.this, "Logged out", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(Account.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void goEditProfile(View view) {
        Intent intent= new Intent(Account.this, EditAccount.class);
        intent.putExtra("next_page", "account");
        startActivity(intent);
        finish();
    }

    public void goAddress(View view) {
        Intent intent= new Intent(Account.this, AddAddress.class);;
        intent.putExtra("next_page", "account");
        startActivity(intent);
        finish();
    }
}
